package com;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@SpringBootApplication
public class FalabellaSpidermanTestApplication {
	
	static String[] getMovieTitles(String substr, int pageNumber) throws UnsupportedOperationException, IOException {
		
		//Creating a HttpClient object
	    CloseableHttpClient httpclient = HttpClients.createDefault();

	    //Creating a HttpGet object
	    HttpGet currencyURL = new HttpGet("https://jsonmock.hackerrank.com/api/movies/search/?Title=" + substr + "&page=" + pageNumber);

	    //Executing the Get request
	    HttpResponse httpresponse = httpclient.execute(currencyURL);

	    // Reading the content of the HTTP request.
	    Scanner sc = new Scanner(httpresponse.getEntity().getContent());
	    // Extract only the values from the response using sub-strings.
	    // Creating variables to store the raw response.
	    String extractedResponse = null;
	    // Creating a JSON Object to parse the raw string into JSON format.
	    JsonObject responseObject = null;
	    // Storing the 'data' part of the JSON Object into a JSON Array.
	    JsonArray responseData = null;
	    // Storing the list of titles into an ArrayList.
	    List<String> titlesList = new ArrayList<String>();

	    while(sc.hasNext()) {
	    	// Reading the HTTP Client's URL response.
	    	extractedResponse = sc.nextLine();
	    	// Converting into JSON object.
	    	responseObject = new Gson().fromJson(extractedResponse, JsonObject.class);
	    	// Extracting specific data from the JSON Object.
	    	responseData = responseObject.getAsJsonArray("data");
	    }
	    sc.close();

	    // Acquiring titles from JSON Array and storing into the list.
	    for(int i = 0; i < responseData.size(); i++) {
	    	JsonObject movieInfo = (JsonObject) responseData.get(i);
	    	String movieTitle = movieInfo.get("Title").toString().replace("\"", "");
	    	titlesList.add(movieTitle);
	    }
	    // Populating the array with the info from the list.
	    String[] sortedArray = new String[titlesList.size()];
	    for(int x = 0; x < sortedArray.length; x++) {
	    	sortedArray[x] = titlesList.get(x);
	    }
	    // Sorting the array in ascending order.
	    Arrays.sort(sortedArray);
	    // Returning the array as sorted.
		return sortedArray;
	}

	public static void main(String[] args) throws UnsupportedOperationException, IOException {
		SpringApplication.run(FalabellaSpidermanTestApplication.class, args);
		FalabellaSpidermanTestApplication x = new FalabellaSpidermanTestApplication();
		String[] spidermanTitles = x.getMovieTitles("spiderman", 1);
		for(int y = 0; y < spidermanTitles.length; y++) {
			System.out.println("Title: " + spidermanTitles[y]);
		}
		
	}

}
